Bioparser
---
A utility to parse questions from the Campbell Biology 8th edition textbook.

Sample input files are at http://apownage.yolasite.com/ap-biology-notes.php. Simply copy and paste into the text box, and hit Parse.

Output file has question and answer separated by ";;;"s and individual question-and-answer blocks separated by "~~~"s. 

This can be imported into programs like Quizlet by setting the appropriate delimiters.

Bioparser is licensed under CC-BY-SA. E-mail me at raj.ksvn at gmail dot com for suggestions, questions, comments, or concerns.

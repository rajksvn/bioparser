'use strict';
function select_text(container_id) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(container_id));
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById(container_id));
        window.getSelection().addRange(range);
    }
}

function parse() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json', 
        url: '/parse',
        data: JSON.stringify({
            'questions':$('#questions').val()
        }),
        success: (function(data) {
            console.log('Parse successful.');
            var parsed = data['parsed'];
            $('#result').text(parsed);
            $('#select').show();
        }),
        error: (function(data) {
            console.log('Parse failure.');
            console.log(data);
            var parsed = 'Oh no! An unexpected error occurred. Try again and contact raj.ksvn@gmail.com if Bioparser still doesn\'t work.';
            $('#result').text(parsed);
            $('#select').hide();
        })
    })
}

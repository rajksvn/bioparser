import re
from flask import Flask, render_template, request, redirect, url_for, send_from_directory, jsonify
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/parse', methods = ['POST'])
def parse():
    packet = request.json['questions']
    r = re.compile(r'(\d?\d\).*?)\nAnswer', re.DOTALL)
    questions = r.findall(packet)
    r = re.compile(r'Answer:\s(.*)')
    answers = r.findall(packet)

    parsed = ''

    for i in range(0, len(questions)):
        parsed += questions[i] + ';;;'
        parsed += answers[i]
        parsed += '~~~\n'

    return jsonify({
        'parsed':parsed
    })

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = int('5000'), debug = True)
